const jwt = require('jsonwebtoken')
const bcript =require('bcrypt')
const Usuario = require('../modelos/usuarios')

const obtenerAdministrador=(solicitud,respuesta)=>{
    jwt.verify(solicitud.token,'clave-secreta',(error,datosUsuario)=>{
        if (error) {
            respuesta.send('ha ocurrido un error de envio')
        }else{
            respuesta.json({
               mensaje:'datos correctos del token',
               datosUsuario: datosUsuario
            })
        }
    })
    respuesta.send('biemvenido al  portal de administradores')
}
const login=(solicitud,respuesta)=>{
    Usuario.findOne({email:solicitud.body.email},(error,resultado)=>{
        if (error) {
            respuesta.send('ha ocurrido un error en el login'+error)
        }else{
            if (resultado) {
                if (bcript.compareSync(solicitud.body.contraseña,resultado.contraseña)) {
                    jwt.sign({usuario:resultado},'clave-secreta',(error,tokenGenerado)=>{
                        respuesta.send({token:tokenGenerado})
                    })
                }else{
                    respuesta.send('contraseña incorecta')
                }
            }else{
                respuesta.send('no existe el usuario en la base de datos')
            }
        }
    })
}
const registro=(solicitud,respuesta)=>{
    const usuario = new Usuario({
        nombre:solicitud.body.nombre,
        email:solicitud.body.email,
        contraseña: bcript.hashSync(solicitud.body.contraseña,10)//encriptar la contraseña primero parametro es el valor y el segundo el numero de saltos para encriptar la contraseña
    })
    usuario.save((error,resultado)=>{
        if (error) {
            respuesta.send('ha ocurrido un error en registro'+error)
        }else{
            respuesta.send('usuario registrado correctamente')
        }
    })
}

module.exports={obtenerAdministrador,login,registro}