const mongoose=require('mongoose')

const Schema=mongoose.Schema

const usuarioEsquema=new Schema({
    nombre:String,
    email:String,
    contraseña:String
})

const Usuario=mongoose.model('Usuario',usuarioEsquema)//primer parametro el nombre del esquema y el segundo es el esquema

module.exports=Usuario