const verificarToken=(solicitud,respuesta,siguiente)=>{
    console.log('accediendo a la funcion middleware')
    const autorizar_header=solicitud.headers['authorization']
    if (autorizar_header!==undefined) {
        //console.log('su token es : '+autorizar_header)
        const token = autorizar_header.split(" ")[1]//separar solo el token de la palabra bearer
        solicitud.token=token
        siguiente()
    }else{
        console.log('no ingreso el token o no es correcto')
    }
    
}

exports.verificarToken=verificarToken