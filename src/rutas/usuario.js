const express= require('express')
const rutas=express.Router()
const usurioControlador=require('../controladores/usuarios')
const verificarToken=require('../middlewares/verificarToken')


rutas.get('/administrador',verificarToken.verificarToken,usurioControlador.obtenerAdministrador)
rutas.post('/login',usurioControlador.login)
rutas.post('/registro',usurioControlador.registro)

module.exports=rutas
