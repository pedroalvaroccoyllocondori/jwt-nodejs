const express = require('express')
const aplicacion=express()
const usuarioRutas=require('./rutas/usuario')

require('./connection')

aplicacion.set('port',4000)

//middleware
aplicacion.use(express.urlencoded({extended:false}))
aplicacion.use(usuarioRutas)


aplicacion.listen(aplicacion.get('port'),()=>{
    console.log("mi aplicacion esta en el puerto :"+aplicacion.get('port'))
})